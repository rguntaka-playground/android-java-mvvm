package com.rgplay.architecturepattern.mvvm.injava.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.rgplay.architecturepattern.mvvm.injava.model.ComputerLanguage;

public class MainViewModel extends ViewModel {

    LiveData<String> liveData;
    public MutableLiveData<String> mutableLiveData; // we can set/ post live data

    // linking ViewModel and Model
    private ComputerLanguage getComputerLanguageFromDB(){
        return new ComputerLanguage("Java","Android");
    }

    // linking ViewModel with Activity (View)
    public void getComputerLanguage(){
        String computerLanguage = getComputerLanguageFromDB().getLanguage();
        mutableLiveData.setValue(computerLanguage);
    }
}

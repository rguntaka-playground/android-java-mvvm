package com.rgplay.architecturepattern.mvvm.injava.model;

/*
 This is a Model class in this MVC architecture pattern example
 */
public class ComputerLanguage {

    // private access ensures that the variables can't be touched directly
    private String language;
    private String mobilePlatform;

    public ComputerLanguage (String language, String mobilePlatform){
        this.language = language;
        this.mobilePlatform = mobilePlatform;
    }

    // access to ComputerLanguage model happens only through getters()
    public String getLanguage(){
        return this.language;
    }

    public String getMobilePlatform(){
        return this.mobilePlatform;
    }
}

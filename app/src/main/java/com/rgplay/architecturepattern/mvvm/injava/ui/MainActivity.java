package com.rgplay.architecturepattern.mvvm.injava.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.rgplay.architecturepattern.mvvm.injava.R;
import com.rgplay.architecturepattern.mvvm.injava.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {



    // Note that ViewModel is not responsible for delivering the data back to Activity/ View, it
    // (cont) will just fetch required data from Model & notify View (Activity) via Observable.
    // (cont) That's said, its the responsibility of the Activity to pick the data published by ViewModel
    MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());

        //dataBinding = true of gradle is linked this below line
        View view = binding.getRoot();
        setContentView(view);

        // Linking Activity with ViewModel
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        // View binding will do the job of observing
        binding.setViewModel(mainViewModel);
        binding.setLifecycleOwner(this);
    }
}